#importowanie modulu
import easygui
#przygotowanie danych do okna
pytanie = 'Kto wygra MŚ 2018?'
tytul = 'MŚ 2018'
przyciski = ['Polska','Niemcy','Brazylia','Włochy']
#uruchomienie okna i przypisanie
#wyniku do zmiennej
wybor = easygui.buttonbox(pytanie, tytul, przyciski)
#wyswietlenie komunikatu w zaleznosci od odp
if wybor == 'Włochy':
    komunikat = 'Oni przecież nie grają!'
elif wybor != 'Polska':
    komunikat = 'Stary! Nie wierzysz w nas?'
else:
    komunikat = 'Do boju Polsko!'

easygui.msgbox(komunikat)
