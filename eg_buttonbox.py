#importowanie modulu
import easygui
#przygotowanie danych do okna
pytanie = 'Czy lubisz placki?'
tytul = 'Pytanie na śniadanie'
przyciski = ['Tak','Nie','Nie wiem']
#uruchomienie okna i przypisanie
#wyniku do zmiennej
#struktura buttonbox
#
#easygui.buttonbox('Treść pytania', 'Tytuł na pasku', ['Lista', 'przyciskow'])
#!!!! Tekst na przycisku jest zwracany jako wartość okna
#
wybor = easygui.buttonbox(pytanie, tytul, przyciski)
#wyswietlenie odpowiedzi w oknie komunikatu
#struktura msgbox
#
#easygui.msgbox('Treść pytania', 'Tytuł na pasku', 'Tekst na przycisku')
#!!!! Tekst na przycisku jest zwracany jako wartość okna
#
easygui.msgbox('Dziękuję. Wybrałeś: ' + wybor)
