#import modulu losujacego
import random
#losowanie liczby losowo od 1 do 10
sekret = random.randint(1,10)

propozycja = 0;
proba = 0;

while propozycja != sekret and proba < 6:
    propozycja = int(input('Podaj liczbę 1-10: '))
    if propozycja < sekret:
        print('Celowałeś za nisko')
    elif propozycja > sekret:
        print('Celowałeś za wysoko')

    proba = proba + 1


#zadziała gdy liczba jest ok
if propozycja == sekret:
    print('Gratulacje')
#zadziała gdy liczba jest zła
else:
    print('Sorry. Nie udało się. Wykorzystałęś wszystkie próby')
    print('Poprawna liczba to %s.' % sekret)

print('The end')
